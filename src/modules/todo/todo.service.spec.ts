import { ITodo, ITodoWithoutCompleted } from './interfaces';
import TodoService from './todo.service';
import {IDB} from "../../db/interfaces";

let todoService: TodoService;
let dbFake: IDB<ITodo> = {
    get: jest.fn(),
    getAll: jest.fn(),
    add: jest.fn(),
    edit: jest.fn(),
    remove: jest.fn()
}

describe('add todo task', () => {
    beforeEach(() => {
        todoService = new TodoService(dbFake);
    });

    it('Adding a new todo succeed if list exist', () => {
        const todoTest: ITodoWithoutCompleted = {
            listId: "unit-test",
            task: "make initial commit"
        }
        const checkIfListExist = jest.spyOn(TodoService.prototype as any, 'checkIfListExist');
        checkIfListExist.mockResolvedValue(true);

        todoService.add(todoTest);
        
        expect(checkIfListExist).toBeCalled();
        expect(dbFake.add).toBeCalledWith(expect.objectContaining({...todoTest, isCompleted: false}));
    });
});

describe('edit todo task', () => {
    beforeEach(() => {
        todoService = new TodoService(dbFake);
    });

    it('Editing existing task succeed', () => {
        const newTodo: ITodo = {
            listId: 'unit-test',
            task: 'edit task',
            isCompleted: false
        }
        const id: string = 'some id';
        const checkIfListExist = jest.spyOn(TodoService.prototype as any, 'checkIfListExist');
        todoService.safeGetById = jest.fn().mockReturnValue({listId: 'unit-test', task: 'old task name', isCompleted: true});
        
        todoService.edit(id, newTodo);
        
        expect(checkIfListExist).toBeCalled();        
        expect(dbFake.edit).toBeCalledWith({...newTodo, id});
    });

    it('edit task with missing list id failes', () => {
        const newTodo: ITodo ={
            listId: 'not exist list id',
            task: 'no listId',
            isCompleted: true
        }
        const id: string = 'some id';
        const checkIfListExist = jest.spyOn(TodoService.prototype as any, 'checkIfListExist');
        checkIfListExist.mockImplementation(() => {
            throw new Error('Mock threw error');
        });

        expect(() => todoService.edit(id, newTodo)).toThrow('Mock threw error');
        expect(dbFake.edit).not.toBeCalledTimes(3); //improve this!
    });
});

describe('edit date of todo task', () => {
    beforeEach(() => {
        todoService = new TodoService(dbFake);
    });

    it('edit finish date when it is undefined', () => {
        const todo: ITodo ={
            listId: 'not exist list id',
            task: 'no listId',
            isCompleted: true
        }
        const id: string = 'some id';
        todoService.safeGetById = jest.fn().mockReturnValue(todo);

        expect(() => todoService.editDate(id, undefined)).toThrow("finishDate is undefined"); //i think that's too specific
        expect(todoService.safeGetById).toBeCalled();
    });

    it('edit date with valid format of date', () => {
        const todo: ITodo ={
            listId: 'not exist list id',
            task: 'no listId',
            isCompleted: true
        }
        const id: string = 'some id';
        const newDate = new Date(Date.now());
        todoService.edit = jest.fn();
        todoService.safeGetById = jest.fn().mockReturnValue(todo);
        todo.finishDate = newDate;

        todoService.editDate(id, newDate);

        expect(todoService.edit).toBeCalledWith(id, todo);
    });

    it('edit date with invalid format of date', () => {
        const todo: ITodo ={
            listId: 'not exist list id',
            task: 'no listId',
            isCompleted: true
        }
        const id: string = 'some id';
        const newDate = new Date(111,111,111);
        todoService.safeGetById = jest.fn().mockReturnValue(todo);

        expect(() => todoService.editDate(id, newDate)).toThrow("Invalid date formatting"); //i think that's too specific
    });
});

describe('toggle todo', () => {
    it('change the toggle of todo to completed', () => {
        todoService = new TodoService(dbFake);
        const todo: ITodo ={
            listId: 'not exist list id',
            task: 'no listId',
            isCompleted: false
        }
        const id: string = 'some id';
        todoService.safeGetById = jest.fn().mockReturnValue(todo);
        const checkIfListExist = jest.spyOn(TodoService.prototype as any, 'checkIfListExist');
        checkIfListExist.mockResolvedValue(true);
        todoService.edit = jest.fn();
        todo.isCompleted = true;

        todoService.toggle(id);

        expect(todoService.edit).toBeCalledWith(id, todo);
    });
});

it('deleting todo task by id succeed', () => {
    todoService = new TodoService(dbFake);
    const id: string = 'some id';

    todoService.remove(id);

    expect(dbFake.remove).toBeCalledWith(id);
});

describe('get todos', () => {
    beforeEach(() => {
        todoService = new TodoService(dbFake);
    });

    it('get todo by id', () => {
        const id: string = 'some id';
        todoService.getByID(id);

        expect(dbFake.get).toBeCalledWith(id);
    });

    it('get all todos', () => {
        todoService.getAll();

        expect(dbFake.getAll).toBeCalled();
    });
});


//Bonus
it('remove date succeed', () => {
    todoService = new TodoService(dbFake);
    const todo: ITodo ={
        listId: 'not exist list id',
        task: 'no listId',
        isCompleted: false,
        finishDate: new Date(Date.now())
    }
    const id: string = 'some id';
    todoService.safeGetById = jest.fn().mockReturnValue(todo);
    delete todo.finishDate;

    todoService.removeDate(id);

    expect(dbFake.edit).toBeCalledWith({...todo, id});
});

it('safe get not existing todo by id failes', () => {
    todoService = new TodoService(dbFake);
    dbFake.get = jest.fn().mockReturnValue(null);
    const id: string = 'hello';

    expect(() => todoService.safeGetById(id)).toThrow(`Todo with this id '${id}' does not exist.`);
});

