module.exports = {
    transform: {
      "^.+\\.ts?$": "ts-jest",
    },
    roots: ['<rootDir>/src'],
    testEnvironment: 'node',
    verbose: true,
    testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.(js?|ts?)$",
    testPathIgnorePatterns: ["/lib/", "/node_modules/"],
    moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
    collectCoverage: true,
  };
  